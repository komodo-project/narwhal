# Narwhal Browser 🐋

Welcome to the Narwhal Browser, a cutting-edge web browser that combines speed, efficiency, and user privacy to deliver a seamless browsing experience. In a digital age where connectivity is key, Narwhal Browser stands out as a beacon of innovation, offering users a fast and secure gateway to the vast world of the internet.

# Contributing Guidelines 🤝

Since Narwhal is an open-source project, users can help to improve it.
To help with the project, follow these guidelines:

- **Branch Naming:** Name your branches accordingly to what changes they make.
- **Branching:** Never commit to the main branch! Create another and submit a merge request. It will get reviewed.
- **Elaborate Requests:** Include a lot of information in issues and pull requests. The more the merrier!
